﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DataModelConstants
{
	public static Dictionary<int, string> TEAM_DEFINITION = new Dictionary<int, string>
	{
		{0, "prop"},
		{1, "hooker"},
		{2, "prop"},
		{3, "lock"},
		{4, "lock"},
		{5, "flanker"},
		{6, "flanker"},
		{7, "number-eight"},
		{8, "scrum-half"},
		{9, "out-half"},
		{10, "winger"},
		{11, "centre"},
		{12, "centre"},
		{13, "winger"},
		{14, "full-back"}
	};

	public static Vector2 ATHLETE_DATA_VIEW_LOCAL_POSITION = new Vector3 (190, -160, 0);
	public static Vector2 TEAM_DATA_VIEW_LOCAL_POSITION = new Vector3 (0.5f, 0, 0);
}
