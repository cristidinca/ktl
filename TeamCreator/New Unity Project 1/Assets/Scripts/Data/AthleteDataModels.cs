﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;

[System.Serializable]
public class Athletes 
{
	public List<Athlete> athletes;

	public static Athletes CreateFromJSON (string jsonString)
	{
		return JsonUtility.FromJson<Athletes> (jsonString);
	}
}

[System.Serializable]
public class Athlete 
{
	public string name;				//"name": "Kevin McLaughlin",
	public string avatar_url;		//"avatar_url": "https://cdn.soticservers.net/tools/images/players/photos/2013/irfu/126/295x300/9853.jpg",
	public int star_rating;			//"star_rating": 5,
	public string country;			//"country": "Ireland",
	public string last_played;		//"last_played": "Sat Nov 05 2016 15:00:00 GMT+0000 (UTC)",
	public List<string> positions;	/*"positions": 
									[
										"full-back",
										"lock"
									],*/
	public bool is_injured;			//"is_injured": false,
	public int id;					//"id": 100
}
