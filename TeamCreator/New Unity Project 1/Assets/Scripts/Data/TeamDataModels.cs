﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrentTeam
{
	public CurrentTeam ()
	{
		team = new Dictionary<Key, Athlete> ();
		foreach (KeyValuePair<int, string> kvp in DataModelConstants.TEAM_DEFINITION) 
		{
			var newKey = new Key(kvp.Key, kvp.Value);
			team.Add (newKey, null);
		}
	}

	public Dictionary<Key, Athlete> team;
}

public class Key 
{
	public int index;
	public string position;

	public Key (int _index, string _position)
	{
		index = _index;
		position = _position;
	}
}

