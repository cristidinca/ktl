﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class AppManager : MonoBehaviour 
{
	static AppManager instance = null;

	public DummyDataView dummyData = null;

	public static AppManager Instance
	{
		get 
		{
			if (instance == null)
			{
				Debug.LogWarning ("AppManager insatnce not initialised yet!");
			}
			return instance;
		}
	}

	public TextAsset JSON;

	public GameObject athleteDataViewPrefab;
	public GameObject teamAthleteDataViewPrefab;

	public Transform athleteScrollViewRoot;
	public Transform teamScrollViewRoot;

	private Athletes athletesJSON;
	//[HideInInspector]
	public Athlete selectedAthlete = null;
	[HideInInspector]
	public Sprite selectedAthleteAvatar = null;
	private CurrentTeam currentTeam = null;

	public int index = -1;
	public string position = string.Empty;

	// Use this for initialization
	void Awake () 
	{
		instance = this;
	}
	void Start () 
	{
		//TODO check for null fields;

		athletesJSON = Athletes.CreateFromJSON (JSON.ToString());
		currentTeam = new CurrentTeam ();

		GameObject athleteDataViewGO = null;
		float athleteDataViewHeight = athleteDataViewPrefab.GetComponent<RectTransform> ().rect.height;
		float athleteDataViewWidth = athleteDataViewPrefab.GetComponent<RectTransform> ().rect.width;

		for (int i = 0; i < athletesJSON.athletes.Count; i++)
		{
			athleteDataViewGO = (GameObject)Instantiate (athleteDataViewPrefab);
			athleteDataViewGO.GetComponent<AthleteDataView>().InitValues(athletesJSON.athletes[i]);
			athleteDataViewGO.transform.SetParent (athleteScrollViewRoot, true);
			athleteDataViewGO.transform.localPosition = DataModelConstants.ATHLETE_DATA_VIEW_LOCAL_POSITION;
			athleteDataViewGO.transform.localPosition -= new Vector3 (0, i * athleteDataViewHeight, 0);
		}
		athleteScrollViewRoot.GetComponent<RectTransform> ().sizeDelta = new Vector2 (athleteDataViewWidth, athleteDataViewHeight * athletesJSON.athletes.Count);				
	
		GameObject teamAthleteDataViewGO = null;
		int index = 0;
		foreach (KeyValuePair<int, string> kvp in DataModelConstants.TEAM_DEFINITION)
		{
			teamAthleteDataViewGO = (GameObject)Instantiate (teamAthleteDataViewPrefab);
			teamAthleteDataViewGO.transform.SetParent (teamScrollViewRoot, true);
			teamAthleteDataViewGO.transform.localPosition = DataModelConstants.ATHLETE_DATA_VIEW_LOCAL_POSITION;
			teamAthleteDataViewGO.transform.localPosition -= new Vector3 (0, index * athleteDataViewHeight, 0);
			teamAthleteDataViewGO.GetComponent<TeamRosterView> ().index = kvp.Key;
			teamAthleteDataViewGO.GetComponent<TeamRosterView> ().position = kvp.Value;
			index++;
		}
		teamScrollViewRoot.GetComponent<RectTransform> ().sizeDelta = new Vector2 (athleteDataViewWidth, athleteDataViewHeight * DataModelConstants.TEAM_DEFINITION.Count);				
	}
		
	public void AddToCurrentTeam () 
	{
		if (index < 0 || string.IsNullOrEmpty (position))
		{
			Debug.LogError ("OOPS! fields not initialised!");
			return;
		}
			
		List<Key> _keys = currentTeam.team.Keys.ToList ();
		List<Athlete> _values = currentTeam.team.Values.ToList ();

		if (_values.Contains (selectedAthlete))
		{
			Debug.LogError ("Attemtpting to assing an already assinged player!");
			return;
		}

		if (selectedAthlete.is_injured)
		{
			Debug.LogError ("Attemtpting to assing an injured player!");
			return;
		}

		if (selectedAthlete != null && (index > -1 &&  string.IsNullOrEmpty(position) == false))
		{
			foreach (string athletePosition in selectedAthlete.positions)
			{
				if (selectedAthlete.positions.Contains (position))
				{
					for (int i = 0; i < currentTeam.team.Count; i++)
					{
						if (currentTeam.team [_keys [i]] != selectedAthlete && _keys[i].index == index)
						{
							currentTeam.team [_keys [i]] = selectedAthlete;
							List<TeamRosterView> positionsList = new List<TeamRosterView> ();
							teamScrollViewRoot.GetComponentsInChildren (positionsList);
							TeamRosterView target = positionsList.Find (obj => obj.index == _keys[i].index);

							target.UpdateAssignedState (selectedAthlete, selectedAthleteAvatar);
						}
					}
				}
				else
				{
					Debug.LogError ("Atemptting to assing to wrong position!\n" +
						selectedAthlete.name + " plays:" + selectedAthlete.positions.ToString());
				}
			}
		}

		selectedAthlete = null;
		index = -1;
		position = string.Empty;
	}

	public void RemoveFromCurrentTeam () 
	{
		if (index < 0 || string.IsNullOrEmpty (position))
		{
			Debug.LogError ("OOPS! fields not initialised!");
			return;
		}

		List<Key> _keys = currentTeam.team.Keys.ToList ();
		List<Athlete> _values = currentTeam.team.Values.ToList ();

		Key targetKey = _keys.Find (obj => obj.index == index && obj.position == position);


		if (_values.Contains (currentTeam.team [targetKey]) == false)
		{
			Debug.LogError ("OOPS! Player not found in current team, please investigate!!\n" + currentTeam.team [targetKey]);
		}
		else
		{
			currentTeam.team [targetKey] = null;
			List<TeamRosterView> positionsList = new List<TeamRosterView> ();
			teamScrollViewRoot.GetComponentsInChildren (positionsList);
			TeamRosterView target = positionsList.Find (obj => obj.index == targetKey.index);
			target.UpdateAssignedState (null, null);
		}

		selectedAthlete = null;
		index = -1;
		position = string.Empty;
	}
}
