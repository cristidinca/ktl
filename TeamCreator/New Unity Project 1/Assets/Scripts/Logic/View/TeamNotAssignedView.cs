﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class TeamNotAssignedView : MonoBehaviour
{
	public Text positionIndexValue;
	public Text poisitonValue;

	void Start () 
	{
		int index = GetComponentInParent<TeamRosterView> ().index;
		string position = GetComponentInParent<TeamRosterView> ().position;

		UpdateValues (index, position);
	}

	public void UpdateValues(int index, string position) 
	{
		positionIndexValue.text = index.ToString ();
		poisitonValue.text = position;
	}
}
