﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using UnityEngine.EventSystems;

public class TeamRosterView : MonoBehaviour, 
IPointerEnterHandler, 
IPointerExitHandler,

IBeginDragHandler,
IDragHandler,
IEndDragHandler
{
	[HideInInspector]
	public int index = -1;
	[HideInInspector]
	public string position = string.Empty;

	public GameObject assignedPrefab;
	public GameObject notAssignedPrefab;

	GameObject assignedGO = null;
	GameObject notAssignedGO = null;

	void Awake () 
	{
		assignedGO = (GameObject)Instantiate (assignedPrefab);
		assignedGO.transform.SetParent (transform, true);
		assignedGO.transform.localPosition = DataModelConstants.TEAM_DATA_VIEW_LOCAL_POSITION;
		notAssignedGO = (GameObject)Instantiate (notAssignedPrefab);
		notAssignedGO.transform.SetParent (transform, true);
		notAssignedGO.transform.localPosition = DataModelConstants.TEAM_DATA_VIEW_LOCAL_POSITION;
		UpdateAssignedState (null, null);
	}

	public void UpdateAssignedState (Athlete newAthlete, Sprite avatarSprite) 
	{
		bool assigned = newAthlete == null ? false : true;
		assignedGO.SetActive (assigned);
		notAssignedGO.SetActive (!assigned);

		if (assigned)
		{
			UpdateAssignedDataView (newAthlete, avatarSprite);
		}
		else
		{
			notAssignedGO.GetComponent<TeamNotAssignedView> ().UpdateValues (index, position);
		}
	}

	void UpdateAssignedDataView (Athlete newAthlete, Sprite avatarSprite = null) 
	{
		assignedGO.GetComponent<DummyDataView> ().InitValues (newAthlete, avatarSprite);
	}

	void IPointerEnterHandler.OnPointerEnter (PointerEventData eventData)
	{
		AppManager.Instance.index = index;
		AppManager.Instance.position = position;
	}

	void IPointerExitHandler.OnPointerExit (PointerEventData eventData)
	{
		AppManager.Instance.index = -1;
		AppManager.Instance.position = string.Empty;
	}


	void IBeginDragHandler.OnBeginDrag (PointerEventData eventData)
	{
		//needed for drag input detection;
	}
		
	void IDragHandler.OnDrag (PointerEventData eventData)
	{
		//needed for drag input detection;
	}
		
	void IEndDragHandler.OnEndDrag (PointerEventData eventData)
	{
		AppManager.Instance.index = index;
		AppManager.Instance.position = position;
		AppManager.Instance.RemoveFromCurrentTeam ();
	}
}
