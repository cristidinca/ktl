﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Globalization;

public class DummyDataView : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler
{
	public bool usedAsCursor = false;

	[Space(5)]
	public Color enter;
	public Color exit;
	public Color down;
	public Color up;

	[Space(5)]
	public Color inactive;
	public Color selected;
	public Color backgroundColorActive;
	public Color backgroundColorInactive;
	public Image backgroundValue;
	AppManager am;

	public Image avatarValue;
	public Text nameValue;
	public Text starRatingValue;
	public Text countryValue;
	public Text lastPlayedValue;
	public Text positiosValue;
	public Text injuredValue;
	public Text idValue;

	public void InitValues (Athlete jsonAthlete, Sprite avatarSprite = null) 
	{
		nameValue.text = jsonAthlete.name;
		starRatingValue.text = jsonAthlete.star_rating.ToString();
		countryValue.text = jsonAthlete.country;

		DateTime dateTime = DateTime.Now;

		if (DateTime.TryParseExact (jsonAthlete.last_played, "ddd MMM dd yyyy HH:mm:ss 'GMT'K", CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal, out dateTime) == false)
		{
			FormatException e = new FormatException ();
			Debug.LogError ("athlete.last_played: " + jsonAthlete.last_played + " has a problem: \n" + e + "\n" + e.InnerException);
			lastPlayedValue.text = "Not Available";
		}
		else
		{
			lastPlayedValue.text = dateTime.ToShortDateString ();
		}

		for (int j = 0; j < jsonAthlete.positions.Count; j++)
		{
			if (j == 0)
			{
				positiosValue.text = jsonAthlete.positions [0];
			}
			else
			{
				positiosValue.text = positiosValue.text + "\n" + jsonAthlete.positions [j];
			}
		}

		injuredValue.text = jsonAthlete.is_injured.ToString ();
		idValue.text = jsonAthlete.id.ToString ();
		if (avatarSprite != null)
			avatarValue.sprite = avatarSprite;
	}

	// Use this for initialization
	void Start ()
	{
		if (usedAsCursor)
			AppManager.Instance.dummyData = this;
	}

	// Update is called once per frame
	void Update () 
	{
		if (usedAsCursor)
		{
			transform.position = Input.mousePosition;

			if (Input.GetMouseButtonDown (0) && AppManager.Instance.selectedAthlete != null)
			{
				backgroundValue.color = backgroundColorActive;
				avatarValue.color = selected;

				InitValues (AppManager.Instance.selectedAthlete, AppManager.Instance.selectedAthleteAvatar);

				for (int i = 0; i < transform.childCount; i++)
				{
					transform.GetChild (i).gameObject.SetActive (true);
				}
			}

			if (Input.GetMouseButtonUp (0) || AppManager.Instance.selectedAthlete == null)
			{
				backgroundValue.color = backgroundColorInactive;
				avatarValue.color = inactive;
				for (int i = 0; i < transform.childCount; i++)
				{
					transform.GetChild (i).gameObject.SetActive (false);
				}
			}
		}
	}

	void IPointerEnterHandler.OnPointerEnter (PointerEventData eventData)
	{
		if(usedAsCursor == false)
			GetComponent<Image> ().color = enter;
	}

	void IPointerExitHandler.OnPointerExit (PointerEventData eventData)
	{
		if(usedAsCursor == false)
			GetComponent<Image> ().color = exit;
	}

	void IPointerDownHandler.OnPointerDown (PointerEventData eventData)
	{
		if(usedAsCursor == false)
			GetComponent<Image> ().color = down;
	}

	void IPointerUpHandler.OnPointerUp (PointerEventData eventData)
	{
		if(usedAsCursor == false)
			GetComponent<Image> ().color = up;
	}
}
