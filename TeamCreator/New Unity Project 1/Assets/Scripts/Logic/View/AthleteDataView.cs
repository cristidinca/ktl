﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using System;
using System.Globalization;

using UnityEngine.EventSystems;

public class AthleteDataView : MonoBehaviour, 
IPointerEnterHandler,
IPointerExitHandler, 
IPointerDownHandler,
IPointerUpHandler,

IBeginDragHandler,
IDragHandler,
IEndDragHandler
{
	[HideInInspector]
	public Athlete athlete;

	public Image avatarValue;
	public Text nameValue;
	public Text starRatingValue;
	public Text countryValue;
	public Text lastPlayedValue;
	public Text positiosValue;
	public Text injuredValue;
	public Text idValue;

	[Space(5)]
	public Color enter;
	public Color exit;
	public Color down;
	public Color up;

	public void InitValues (Athlete jsonAthlete) 
	{
		nameValue.text = jsonAthlete.name;
		starRatingValue.text = jsonAthlete.star_rating.ToString();
		countryValue.text = jsonAthlete.country;

		DateTime dateTime = DateTime.Now;

		if (DateTime.TryParseExact (jsonAthlete.last_played, "ddd MMM dd yyyy HH:mm:ss 'GMT'K", CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal, out dateTime) == false)
		{
			FormatException e = new FormatException ();
			Debug.LogError ("athlete.last_played: " + jsonAthlete.last_played + " has a problem: \n" + e + "\n" + e.InnerException);
			lastPlayedValue.text = "Not Available";
		}
		else
		{
			lastPlayedValue.text = dateTime.ToShortDateString ();
		}

		for (int j = 0; j < jsonAthlete.positions.Count; j++)
		{
			if (j == 0)
			{
				positiosValue.text = jsonAthlete.positions [0];
			}
			else
			{
				positiosValue.text = positiosValue.text + "\n" + jsonAthlete.positions [j];
			}
		}

		injuredValue.text = jsonAthlete.is_injured.ToString ();
		idValue.text = jsonAthlete.id.ToString ();
		athlete = jsonAthlete;
		StartCoroutine (GetAvatarImage(athlete.avatar_url));
	}

	IEnumerator GetAvatarImage (string url)
	{
		WWW www = new WWW (url);

		yield return www;

		Texture2D texture = new Texture2D (www.texture.width, www.texture.height, TextureFormat.DXT1, false);
		www.LoadImageIntoTexture (texture);
		Rect r = new Rect (0, 0, texture.width, texture.height);
		Sprite img = Sprite.Create (texture, r, avatarValue.transform.position, 100);
		avatarValue.sprite = img;

		www.Dispose ();
	}
		
	void IPointerEnterHandler.OnPointerEnter (PointerEventData eventData)
	{
		GetComponent<Image> ().color = enter;
	}

	void IPointerExitHandler.OnPointerExit (PointerEventData eventData)
	{
		GetComponent<Image> ().color = exit;
	}

	void IPointerDownHandler.OnPointerDown (PointerEventData eventData)
	{
		GetComponent<Image> ().color = down;
	}

	void IPointerUpHandler.OnPointerUp (PointerEventData eventData)
	{
		GetComponent<Image> ().color = up;
	}

	void IBeginDragHandler.OnBeginDrag (PointerEventData eventData)
	{
		AppManager.Instance.selectedAthlete = athlete;
		AppManager.Instance.selectedAthleteAvatar = avatarValue.sprite;
	}
		
	void IDragHandler.OnDrag (PointerEventData eventData)
	{
		//needed for drag input detection;
	}
		
	void IEndDragHandler.OnEndDrag (PointerEventData eventData)
	{
		AppManager.Instance.AddToCurrentTeam ();
	}
}
